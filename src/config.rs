use lazy_static::lazy_static;
use std::env;

macro_rules! env_var {
    ($x:ident) => {
        lazy_static! {
            pub static ref $x: String = env::var(stringify!($x)).expect(concat!(
                "environment variable ",
                stringify!($x),
                " must be set"
            ));
        }
    };
}

env_var!(GITLAB_WEBHOOK_TOKEN);
env_var!(TELEGRAM_BOT_TOKEN);
env_var!(TELEGRAM_GROUP_ID);
