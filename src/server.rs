use crate::config;
use crate::events::Event;
use log::{error, info};
use serde_json::Value;
use std::convert::TryFrom;
use warp::{self, Filter};
#[derive(Debug)]
enum Error {
    TelegramRequestNotSent,
    BadGitlabJson,
}

use Error::*;

fn filter() -> impl Filter<Extract = (impl warp::reply::Reply,), Error = warp::Rejection> {
    let gitlab_token_header = warp::header::exact("X-Gitlab-Token", &config::GITLAB_WEBHOOK_TOKEN);
    let json = warp::body::json::<Value>();
    warp::post2()
        .and(gitlab_token_header)
        .and(json)
        .and_then(|event| {
            handle_payload(event)
                .map(|_| warp::reply::reply())
                .map_err(|_| warp::reject::not_found())
        })
}

pub fn run() {
    warp::serve(filter()).run(([127, 0, 0, 1], 3030));
}

fn handle_payload(json: Value) -> Result<(), Error> {
    Event::try_from(json)
        .map_err(|_| BadGitlabJson)
        .and_then(|evt| {
            evt.notify()
                .map(|response| {
                    info!("Telegram request sent {:?}", response);
                    // TODO: handle bad responses
                    ()
                })
                .map_err(|error| {
                    error!("Telegram request not sent: {}", error);
                    TelegramRequestNotSent
                })
        })
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::read_to_string;

    fn test_response_is_ok(path: &str) {
        let payload = read_to_string(path).expect("file does not exist");
        let json: Value = serde_json::from_str(&payload).expect("failed to parse file as json");
        let res = warp::test::request()
            .method("POST")
            .header("X-Gitlab-Token", &*config::GITLAB_WEBHOOK_TOKEN)
            .path("/")
            .json(&json)
            .reply(&filter());
        assert_eq!(res.status(), 200);
    }

    #[test]
    fn good_request_merge_request() {
        test_response_is_ok("./tests/merge_request.json")
    }

    #[test]
    fn good_request_push() {
        test_response_is_ok("./tests/push.json")
    }

    #[test]
    fn good_request_pipeline() {
        test_response_is_ok("./tests/pipeline.json")
    }

    #[test]
    fn bad_request_missing_header() {
        let payload = read_to_string("./tests/pipeline.json").expect("file does not exist");
        let json: Value = serde_json::from_str(&payload).expect("failed to parse file as json");
        assert!(!warp::test::request()
            .method("POST")
            .path("/")
            .json(&json)
            .matches(&filter()))
    }

    #[test]
    fn bad_request_malformatted_json() {
        assert!(!warp::test::request()
            .method("POST")
            .path("/")
            .header("X-Gitlab-Token", &*config::GITLAB_WEBHOOK_TOKEN)
            .json(&Value::default())
            .matches(&filter()))
    }
}
