mod config;
mod events;
mod server;
use pretty_env_logger;

fn main() {
    let _ = pretty_env_logger::try_init();
    server::run();
}
