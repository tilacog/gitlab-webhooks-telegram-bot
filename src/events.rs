use crate::config::{TELEGRAM_BOT_TOKEN, TELEGRAM_GROUP_ID};
use reqwest;
use serde::Deserialize;
use serde_json::Value;
use std::convert::TryFrom;

#[derive(Deserialize, Debug)]
struct User {
    username: String,
}

#[derive(Deserialize, Debug)]
struct Repository {
    name: String,
    url: String,
}

#[derive(Deserialize, Debug)]
struct MergeRequestDetail {
    id: u64,
    title: String,
    state: String,
    target_branch: String,
    source_branch: String,
    url: String,
}

#[derive(Deserialize, Debug)]
struct Build {
    stage: String,
    name: String,
    status: String,
}

#[derive(Deserialize, Debug)]
struct Commit {
    id: String,
    message: String,
    url: String,
}

#[derive(Deserialize, Debug)]
struct PipelineDetail {
    id: u64,
    status: String,
    #[serde(rename = "ref")]
    branch: String,
}

#[derive(Deserialize, Debug)]
pub struct MergeRequest {
    user: User,
    repository: Repository,
    #[serde(rename = "object_attributes")]
    details: MergeRequestDetail,
}

#[derive(Deserialize, Debug)]
pub struct Push {
    repository: Repository,
    user_name: String,
    commits: Vec<Commit>,
    #[serde(rename = "ref")]
    branch: String,
}

#[derive(Deserialize, Debug)]
pub struct Pipeline {
    user: User,
    merge_request: Option<MergeRequestDetail>,
    commit: Commit,
    builds: Vec<Build>,
    #[serde(rename = "object_attributes")]
    details: PipelineDetail,
}

pub enum Event {
    Push(Push),
    MergeRequest(MergeRequest),
    Pipeline(Pipeline),
}

impl Event {
    pub fn notify(&self) -> Result<reqwest::Response, reqwest::Error> {
        let message: String = self.render_message();
        let client = reqwest::Client::new();
        client
            .post(&format!(
                "https://api.telegram.org/bot{}/sendMessage",
                &*TELEGRAM_BOT_TOKEN
            ))
            .query(&[("chat_id", &*TELEGRAM_GROUP_ID)])
            .query(&[("text", &message)])
            .send()
    }

    fn render_message(&self) -> String {
        match self {
            Event::Push(push) => {
                let mut msg = format!(
                    r#"New Push
User: {}
Branch: {}
Commits [{}]:
"#,
                    push.user_name,
                    push.branch,
                    push.commits.len()
                );
                for commit in &push.commits {
                    msg += &format!(" - {}\n", commit.message)
                }
                msg
            }

            Event::MergeRequest(mr) => format!(
                r#"Merge Request {}: {}

Repository:{}
Author: {}
[{}] -> [{}]

{}"#,
                mr.details.state,
                mr.details.title,
                mr.repository.name,
                mr.user.username,
                mr.details.source_branch,
                mr.details.target_branch,
                mr.details.url
            ),
            Event::Pipeline(ppl) => {
                let merge_request_url = match &ppl.merge_request {
                    Some(mr) => &mr.url,
                    None => "-",
                };
                let mut msg = format!(
                    r#"Pipeline #{} Update
Status:{}
User: {}
Commit:{}
Branch: {}
Merge Request URL: {}
Builds:\n"#,
                    ppl.details.id,
                    ppl.details.status,
                    ppl.user.username,
                    ppl.commit.url,
                    ppl.details.branch,
                    merge_request_url,
                );
                for build in &ppl.builds {
                    msg += &format!(" - {}:{} : {}\n", build.stage, build.name, build.status)
                }
                msg
            }
        }
    }
}

impl TryFrom<Value> for Event {
    type Error = ();
    fn try_from(json: Value) -> Result<Self, Self::Error> {
        let kind = match json.get("object_kind") {
            Some(Value::String(kind)) => kind,
            _ => return Err(()),
        };

        match kind.as_ref() {
            "merge_request" => match serde_json::from_value::<MergeRequest>(json) {
                Ok(mr) => Ok(Event::MergeRequest(mr)),
                Err(_) => Err(()),
            },
            "push" => match serde_json::from_value::<Push>(json) {
                Ok(push) => Ok(Event::Push(push)),
                Err(_) => Err(()),
            },
            "pipeline" => match serde_json::from_value::<Pipeline>(json) {
                Ok(pipeline) => Ok(Event::Pipeline(pipeline)),
                Err(_) => Err(()),
            },
            _ => Err(()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::read_to_string;
    #[test]
    fn parse_valid_json_merge_request() {
        let payload = read_to_string("./tests/merge_request.json").expect("file does not exist");
        serde_json::from_str::<MergeRequest>(&payload)
            .expect("failed to parse json as a MergeRequest struct");
    }

    #[test]
    fn parse_valid_json_merge_request_as_event() {
        let payload = read_to_string("./tests/merge_request.json").expect("file does not exist");
        let json: Value = serde_json::from_str(&payload).expect("failed to parse file as json");
        Event::try_from(json).expect("fialed to parse json as Event");
    }

    #[test]
    fn parse_valid_json_push() {
        let payload = read_to_string("./tests/push.json").expect("file does not exist");
        serde_json::from_str::<Push>(&payload).expect("failed to parse json as a Push struct");
    }

    #[test]
    fn parse_valid_json_push_as_event() {
        let payload = read_to_string("./tests/push.json").expect("file does not exist");
        let json: Value = serde_json::from_str(&payload).expect("failed to parse file as json");
        Event::try_from(json).expect("fialed to parse json as Event");
    }

    #[test]
    fn parse_valid_json_pipeline() {
        let payload = read_to_string("./tests/pipeline.json").expect("file does not exist");
        serde_json::from_str::<Pipeline>(&payload)
            .expect("failed to parse json as a Pipeline struct");
    }

    #[test]
    fn parse_valid_json_pipeline_as_event() {
        let payload = read_to_string("./tests/pipeline.json").expect("file does not exist");
        let json: Value = serde_json::from_str(&payload).expect("failed to parse file as json");
        Event::try_from(json).expect("fialed to parse json as Event");
    }
}
